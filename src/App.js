import { useState } from "react";

import "./styles.css";
import { DragContainer } from "./components/DragContainer";

const ITEMS = [
  {
    image:
      "https://bankimooncentre.org/wp-content/uploads/2020/05/LinkedIn-Icon-Square.png",
    title: "LinkedIn"
  },
  {
    image:
      "https://policyviz.com/wp-content/uploads/2020/12/amazon-logo-square-285x300.jpg",
    title: "Amazon"
  },
  {
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Facebook_logo_%28square%29.png/240px-Facebook_logo_%28square%29.png",
    title: "Facebook"
  },
  {
    image:
      "https://seeklogo.com/images/T/twitter-icon-square-logo-108D17D373-seeklogo.com.png",
    title: "Twitter"
  },
  {
    image:
      "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEU7RFz///8zPVcrNlItOFMwOlQbKkk1Pliusbk3QFn09Pabn6l+g5G8vsRxd4ZcY3ZGT2bExsyKjpolMU4hLkzY2d2+wMfg4eTT1dnz8/SCh5SlqLHKzNFOVWphaHqsr7eJjZpYX3KXmqUUJEZvdIR3fYxASWBIsMz/AAAFL0lEQVR4nO2dW5eqOBBGIQEhXlrbgIjidfT8/5843Ygex1QQdA1Uur/95ENci01CKhdSeB4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPA7EDoZNEP1famvoeep35CNi4oqypr6+f7URUPZQtBJw2DXQtBJw8HwhxuqDWkyeSTOXDUUY0pw8U/wQLJy1TAcUYaj8LGcmLtqKBeU4Uo8lnPXUJOxwgzs7hpGZEczMMo5a6i2lGCmjYLOGtIdTRwYBZ011OSY+xwZBZ01FORj6JkerhqKVcPH0FlDvaQMzXjvrqFHNtIZoeGoIT1zSolG6qoh3c/MjSGb56phsKYEh2Yw9Bw1VIqswrWkCjtpmJDTCl+REi4aig+6CslG6qRhSArST6GThvSI1B8T0f4b9wwHn6TgMrGUd87Q0ssMI5uBa4YJXYP+iQr2JW4ZitCyE3MkQ+HlPy4Z6lNOC8a2h9C7GR60JAhYeYdBTPv5uxrBq+HSWAwvoWYjPRHquW2fYl0neDW08GEue/SCCvTc0kB9f0XNmf7igmGgD7F1nymf2juZEgcMZ+uandBP8ewS+RtKSwQsK/Cj9hEsic7+kCB3wnCkrXH+jogKE7LadONsOFxLy2yiEdX2I1/DbPWWnwOGh/f8HDDcvjsWgWGHwPBFYNghMHwRGHYIDF8Ehh0CwxeBYYfA8EX4G+r7N51rl2vE43vRJZr9Ok1+v3BGvQt1RewdXmu7Qbx0eYN+TfMKDLvgFxgWP90wGi/uN/xI3waGBbl9uGCxf/ifVfnk/KKh58AecAl9FqiB4ay7a3wPGJrAkBswNIEhN2BoAkNuwNAEhtyAoQkMuQFDExhyA4YmMOQGDE1gyA0YmsCQGzA0+QWGx7LIgd8+Gk17Q7Evi5ybHCDiQNTasEoKFktlwGCH2yQiM0ZM6s4gSuofJRw28Q3oNG1FnaElB8MXn/WnT/uBzulJZcC6UXU1FByfTfWHutJh/Vln6xHbMUdF+nJr3mv7qsS9tZk+OUPcC3TKy/o+Q09sihwN6VekLNl3riRGXqk8Z9tMQzIJ1vJJryi39xns8skpOZS/Coa9aUS+FOU/G5Mp7a3iYrksJqOxp2Xk6UslcoyIdGbW54MyJQKptZShKO9GldPOzMrbPwnZ1bRubopvM6UfRL/1yefk0kwZ1mF18x851oZEgqqZ7hkq0hExbxvaODdTepy5b1uJ1Z3i2Jtq0jBvWxlVekkyYWbPWD77QCeGtFNNU54NFnrBMllo+6GVwaWZEtmVeyekk+pkZkL2Whg3U29AT9t37Roc52Zq+XqHP24X96vhEcc69ALLpHbbKmRcm2nbONMJ2pKdbtqmFtWMbzO1Jvn0T21iRtWb/vnfrvIttOU81PF5+q8rweYSdlqPaTvCksnUL0SzC1a3E0ZHln3NF4ntVNo+aRDEA3Vt55/Na71r5MmyEppOk/pqUVLfbg+Zq54Lkba0VD89y8A6awiTzd+umOXS/h36ZEuqmMdbLcNHSyWknh3vhkQtOqaeiAb2vJ/DYnSa6e/lJxkEgZQ6UdP94r54unk371sXhHJf+5nAbFlM4t16NynSh3uRnTXDeQWFSLa257GG5ceAa5QgUDJcFW2+FpjHG81x/aIOoeV23eyzpNl6qxmeAG6ACrXezCdpTWUO091KOapXoUSg9Ww7HsWfaZbl+cV2mGfpIj6eNlIHwmW9G0qE37sUOkku3wtOkq+YEYT0Nz4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAfxb+HvVYrr01NawAAAABJRU5ErkJggg==",
    title: "Tumblr"
  },
  {
    image:
      "https://cdn4.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-instagram-new-square2-512.png",
    title: "Instagram"
  }
];

export default function App() {
  const [isEditing, setIsEditing] = useState(false);

  return (
    <div className="App">
      <div className="container">
        <div
          style={{
            marginBottom: "8px",
            display: "flex",
            justifyContent: "flex-end"
          }}
          className="button"
        >
          <button
            onClick={() => setIsEditing(!isEditing)}
            style={{
              border: "1px solid #e0e0e0",
              borderRadius: "6px",
              background: "none",
              padding: "8px 12px",
              color: "#222"
            }}
          >
            {isEditing ? "Stop" : "Edit"}
          </button>
        </div>
        <DragContainer isActive={isEditing} items={ITEMS} />
      </div>
    </div>
  );
}
