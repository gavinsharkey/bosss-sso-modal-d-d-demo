import React, { useRef } from "react";

export function DragItem(props) {
  const ref = useRef();
  const {
    item,
    onDragStart,
    onDragEnter,
    onDragLeave,
    onDragEnd,
    onDrop,
    isDragActive = false
  } = props;
  const { isBeingDragged, isHoverLeft, isHoverRight, image, title } = item;

  if (!isDragActive) {
    return (
      <div ref={ref} className={`sso-item`}>
        <div className="item-image">
          <img src={image} alt="" />
        </div>
        <h2>{title}</h2>
      </div>
    );
  }

  return (
    <div
      ref={ref}
      draggable
      style={{ opacity: isBeingDragged ? 0.5 : 1 }}
      onDragStart={(e) => onDragStart(e, item, ref.current)}
      onDragEnd={onDragEnd}
      className={`sso-item`}
    >
      <div className="item-image">
        <img src={image} alt="" />
      </div>
      <h2>{title}</h2>
      <div className="drag-icon">
        <i class="fas fa-grip-vertical"></i>
      </div>
      <div
        onDragOver={(e) => e.preventDefault()}
        onDragEnter={() => onDragEnter("left")}
        onDragLeave={onDragLeave}
        onDrop={(e) => onDrop(e, "left")}
        className={`hover hover-left ${isHoverLeft ? "is-active" : ""}`}
      ></div>
      <div
        onDragOver={(e) => e.preventDefault()}
        onDragEnter={() => onDragEnter("right")}
        onDragLeave={onDragLeave}
        onDrop={(e) => onDrop(e, "right")}
        className={`hover hover-right ${isHoverRight ? "is-active" : ""}`}
      ></div>
    </div>
  );
}
