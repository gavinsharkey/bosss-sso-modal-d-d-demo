import React, { useState, useEffect } from "react";
import { DragItem } from "../DragItem";

export function DragContainer(props) {
  const { items: initialItems, isActive } = props;
  const [items, setItems] = useState(() => {
    return initialItems.map((item) => {
      return {
        ...item,
        id: Math.random(),
        isBeingDragged: false,
        isHoverLeft: false,
        isHoverRight: false
      };
    });
  });

  function handleDragStart(e, item, element) {
    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.dropEffect = "move";
    e.dataTransfer.setDragImage(element, e.offsetX, e.offsetY);
    e.dataTransfer.setData("text/plain", JSON.stringify(item));

    setItems((prevItems) => {
      return prevItems.map((currentItem) => {
        if (currentItem.id !== item.id) return currentItem;

        return {
          ...currentItem,
          isBeingDragged: true
        };
      });
    });
  }

  function handleDragEnd() {
    setItems((prevItems) => {
      return prevItems.map((currentItem) => {
        return {
          ...currentItem,
          isBeingDragged: false,
          isHoverLeft: false,
          isHoverRight: false
        };
      });
    });
  }

  function handleDragEnter(item, side) {
    setItems((prevItems) => {
      return prevItems.map((currentItem) => {
        if (currentItem.id !== item.id) return currentItem;

        if (side === "left") {
          return {
            ...currentItem,
            isHoverLeft: true,
            isHoverRight: false
          };
        } else if (side === "right") {
          return {
            ...currentItem,
            isHoverRight: true,
            isHoverLeft: false
          };
        }

        return currentItem;
      });
    });
  }

  function handleDragLeave(item) {
    setItems((prevItems) => {
      return prevItems.map((currentItem) => {
        if (currentItem.id !== item.id) return currentItem;

        return {
          ...currentItem,
          isHoverLeft: false,
          isHoverRight: false
        };
      });
    });
  }

  function handleDrop(dropTargetItem, event, side) {
    const droppedItem = JSON.parse(event.dataTransfer.getData("text/plain"));

    setItems((prevItems) => {
      const newItems = prevItems.filter((item) => item.id !== droppedItem.id);

      const dropTargetIndex = newItems.findIndex(
        (item) => item.id === dropTargetItem.id
      );
      const droppedItemNewIndex =
        side === "left" ? dropTargetIndex : dropTargetIndex + 1;

      newItems.splice(droppedItemNewIndex, 0, droppedItem);
      return newItems;
    });
  }

  return (
    <div className="sso-container">
      {items.map((item) => {
        return (
          <DragItem
            key={item.id}
            item={item}
            isDragActive={isActive}
            onDrop={(e, side) => handleDrop(item, e, side)}
            onDragStart={handleDragStart}
            onDragEnter={(side) => handleDragEnter(item, side)}
            onDragLeave={() => handleDragLeave(item)}
            onDragEnd={() => handleDragEnd(item)}
          />
        );
      })}
    </div>
  );
}
